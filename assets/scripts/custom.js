
"use strict";

//Fifth section (work gallery block): Load more gallery
let gallery = document.querySelector('.link');

gallery.addEventListener('click', function(){
	document.querySelector('.img-section-more').classList.toggle('show');
});

//Fifth section (work gallery block): Work examples
let more = document.querySelector('.link');

more.addEventListener('click', function(e){
  e.preventDefault();
  document.querySelector('.main-gallery').classList.toggle('closed');
  document.querySelector('.all-gallery').classList.toggle('open');
});


//Sixth section: Video
console.clear();
const controls = document.querySelector('.controls');
const video = document.querySelector('.video');
const playButton = document.querySelector('.play-button');
const stopButton = document.querySelector('.pause-button');
 
function pause() {
  playButton.classList.remove('hidden');
  stopButton.classList.add('hidden');
  controls.classList.remove('hide-controls');
  video.pause();
}

function start() {
  playButton.classList.add('hidden');
  stopButton.classList.remove('hidden');
  controls.classList.add('hide-controls');
  video.play();
}

function resetControlState() {
	controls.classList.remove('hide-controls');
}

